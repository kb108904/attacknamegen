package com.example.AtackName;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.example.AttackName.R;

import android.widget.ImageView;
import android.widget.RadioButton;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;


public class HomeActivity extends Activity {
	public final static String EXTRA_MESSAGE = "com.example.AttackName.MESSAGE";
	public static boolean flag = true;
	public static boolean short_flag = false;
	public static boolean long_flag = false;
	public static boolean rand_flag = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		TextView textView = (TextView) findViewById(R.id.info_text);
		Animation text_animate = AnimationUtils.loadAnimation(this,R.anim.fade_in);
		
		
		BitmapHelper b = new BitmapHelper();
		ImageView mImageViewBack = (ImageView) findViewById(R.id.backing);
		

		mImageViewBack.setImageBitmap(b.decodeSampledBitmapFromResource(getResources(), R.drawable.backdrop, b.Screen(this).x/2, b.Screen(this).y/2));
		
		if (flag) {
			textView.startAnimation(text_animate);
		}

	}
	
	public void onBackPressed(){
		Log.i("Back Press", "back press detected");
		super.onBackPressed();
		super.finish();
	}

	public void onStop() {
		super.onStop();
		flag = false;
	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	// getMenuInflater().inflate(R.menu.home, menu);
	// return true;
	// }
	public void onRadioButtonClicked(View view) {
		// Is the button now checked?
		boolean checked = ((RadioButton) view).isChecked();
		// Check which radio button was clicked
		switch (view.getId()) {
		case R.id.lon:
			if (checked)
				long_flag = true;
			short_flag = false;
			rand_flag = false;
			System.out.println("Long Flag set");
			break;
		case R.id.shor:
			if (checked)
				long_flag = false;
			short_flag = true;
			rand_flag = false;
			System.out.println("Short Flag set");
			break;
		case R.id.ran:
			if (checked)
				long_flag = false;
			short_flag = false;
			rand_flag = true;
			System.out.println("Rand Flag set");
			break;
		}
	}

	public void Generate_Name(View view) {

		Intent intent = new Intent(this, DisplayNameActivity.class);

		String message = null;

		message = randomName();

		char[] stringArray = message.toCharArray();
		stringArray[0] = Character.toUpperCase(stringArray[0]);
		message = new String(stringArray);

		intent.putExtra(EXTRA_MESSAGE, message);
		startActivity(intent);

	}

	public String randomName() {

		String Name = "";
		String sys = "";
		int num = 0;
		if (long_flag) {
			System.out.println("Long Flag is set, setting long name");
			return Name = Name + adj() + " " + action() + " " + noun()+ " " +attack();
		}
		if (short_flag) {
			System.out.println("Short Flag is set, setting short name");
			return Name = Name + noun()+ " " +attack();
		}
		if (rand_flag) {
			System.out.println("Random Flag is set, generating random number");
			num = (randnum(1000) + 1000);// gets number between 1000 and 2000
		}
		
		if((((num / 100) & 1) == 0) && (((num / 10) & 1) == 0) && (((num & 1) == 0))){ //if all digits are even
			
			return Name = Name + attack() + " of the " + adj() + " " + noun();
			
		}
		if (((num / 100) & 1) == 0) { // if num/100 is even call adj
			Name += adj();
			sys += "adj ";
		}
		if (((num / 10) & 1) != 0) { // if num/10 is not even call verb

			if (Name.length() > 0) {
				Name += " ";
			}
			Name += action();
			sys += "action ";
		}
		if ((num & 1) == 0) { // if num is even call noun()
			if (Name.length() > 0) {
				Name += " ";
			}
			Name += noun();
			sys += "noun ";
		}

		if (Name.length() == 0) { // if no name yet call adj()
			Name += adj();
			sys += "default ";
		}
		Name = Name + " " +attack();
		System.out.println("[[[[[[[[[[[[[[[[[[[" + sys + "]]]]]]]]]]]]]]]]]]");
		return Name;

	}

	// returns num between 1-max
	public int randnum(int max) {
		int ran = (int) (Math.random() * ((max - 0) + 1));
		if(ran == 0){ran = 1;}
		System.out.println(ran + "<-randon:********:max->" + max);
		return ran;
	}

	public String parser(String str) {
		String[] temp = str.split("\\[");
		String[] word = temp[1].split("\\%", 2);
		return word[0];
	}

	public String noun() {
		String line = null;
		String noun = null;
		BufferedReader animalbuff = null;
		BufferedReader materialbuff = null;
		BufferedReader astrobuff = null;
		int RANDNUM_Animal = randnum(66);
		int RANDNUM_Material = randnum(37);
		int RANDNUM_Astro = randnum(140);
		int ran = randnum(3);
		try {
			animalbuff = new BufferedReader(new InputStreamReader(getAssets().open(
					"Animals")));
			materialbuff = new BufferedReader(new InputStreamReader(getAssets().open(
					"Materials")));
			astrobuff = new BufferedReader(new InputStreamReader(getAssets().open(
					"Astrology")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // 3299 lines

		if (ran == 1) {
			for (int i = 0; i < RANDNUM_Animal; i++) {
				try {
					line = new String(animalbuff.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else if (ran == 2) {
			for (int i = 0; i < RANDNUM_Material; i++) {
				try {
					line = new String(materialbuff.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		else if (ran == 3) {
			for (int i = 0; i < RANDNUM_Astro; i++) {
				try {
					line = new String(astrobuff.readLine());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		noun = line;
		animalbuff = null;
		materialbuff = null;
		astrobuff = null;
		//noun = parser(line);
		return noun;
	}

	public String adj() {String line = null;
	String adj = null;
	BufferedReader abuff = null;
	int RANDNUM = randnum(698);
	try {
		abuff = new BufferedReader(new InputStreamReader(getAssets().open(
				"adjs")));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	for (int i = 0; i < RANDNUM; i++) {
		try {
			line = new String(abuff.readLine());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	abuff = null;
	adj = parser(line);
	return adj;
	}

	public String action() {
		String line = null;
		String verb = null;
		BufferedReader vbuff = null;
		int RANDNUM = randnum(114);
		try {
			vbuff = new BufferedReader(new InputStreamReader(getAssets().open(
					"Action")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (int i = 0; i < RANDNUM; i++) {
			try {
				line = new String(vbuff.readLine());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		vbuff = null;
		verb = line;

		if ((verb.charAt(verb.length() - 1)) == 'e') {
			verb = verb.replaceFirst("e\\b", "ing");

		} else {
			verb += "ing";
		}

		return verb;
	}
	public String attack() {
		String line = null;
		String attack = null;
		BufferedReader abuff = null;
		int RANDNUM = randnum(19);
		try {
			abuff = new BufferedReader(new InputStreamReader(getAssets().open(
					"Attack")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < RANDNUM; i++) {
			try {
				line = new String(abuff.readLine());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		abuff = null;
		attack = line;
		return attack;
	}

	public void openurl(View view) {
		Intent browserIntent = new Intent(Intent.ACTION_VIEW,
				Uri.parse("http://blacktribbles.podomatic.com/rss2.xml"));
		startActivity(browserIntent);
	}

}
